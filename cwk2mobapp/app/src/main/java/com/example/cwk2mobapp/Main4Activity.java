package com.example.cwk2mobapp;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import static android.provider.BaseColumns._ID;
import static com.example.cwk2mobapp.Constants.AVAILABILITY;
import static com.example.cwk2mobapp.Constants.DESCRIPTION;
import static com.example.cwk2mobapp.Constants.NAMEOFPRODUCT;
import static com.example.cwk2mobapp.Constants.PRICE;
import static com.example.cwk2mobapp.Constants.TABLE_NAME_AVAILABILITY;
import static com.example.cwk2mobapp.Constants.TABLE_NAME_INGREDIENTS;
import static com.example.cwk2mobapp.Constants.WEIGHT;

public class Main4Activity extends AppCompatActivity {
    private AvailabilityData available;
    private static final String TAG = "AvailabileProd";
    private ListView availableShow;
    private Button saveBtn;
    private static String[] FROM = {_ID, NAMEOFPRODUCT, WEIGHT, PRICE, DESCRIPTION};
    private static String ORDER_BY = NAMEOFPRODUCT;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        available = new AvailabilityData(Main4Activity.this);

        setContentView(R.layout.activity_main4);
        availableShow = (ListView)findViewById(R.id.availableProductsList);
//        final Cursor cursor = getAvailableIngredients();
//        showAvailability(cursor);

    }
    public void onUpdate(int newAvailability, int idIn) {
        //update table_name set availability = 0 where _ID = 1
        SQLiteDatabase db = available.getWritableDatabase();
        String query = " UPDATE " + TABLE_NAME_AVAILABILITY + " set " + AVAILABILITY + " = " + newAvailability + " where _ID = " + idIn;
        db.execSQL(query);
    }

    public String findAvailableProduct(String name) {
        SQLiteDatabase db = available.getReadableDatabase();
        String[] fieldList = {_ID};
        String where = Constants.NAMEOFPRODUCT + " = '" + name + "'";
        // SELECT * from availability WHERE Name like '%LETTUCE%'
        Cursor cursor = db.query(TABLE_NAME_INGREDIENTS, fieldList, where,null, null, null,null);
        if(cursor != null) {
            while (cursor.moveToNext()) {
                String id = cursor.getString(0);
                cursor.close();
                return id;
            }
        }
        cursor.close();
        return null;
    }

    public Boolean saveData(String name, Double weight, Double price, String description, Double availability) {
        SQLiteDatabase db = available.getWritableDatabase();
//        onCreate(db);
        ContentValues contentValues = new ContentValues();
        contentValues.put(NAMEOFPRODUCT, name);
        contentValues.put(WEIGHT, weight);
        contentValues.put(PRICE, price);
        contentValues.put(DESCRIPTION, description);
        contentValues.put(AVAILABILITY, availability);
        Log.d(TAG, "ADD DATA: ADDING " + name + "to " + TABLE_NAME_AVAILABILITY);
        String id = findAvailableProduct(name);
        if(id == null) {
            long result = db.insert(TABLE_NAME_AVAILABILITY, null, contentValues);
            if (result == -1) {
                System.out.println(" false");
                return false;
            } else {
                System.out.println(" true");
                return true;
            }
        } else {
            onUpdate(1, Integer.parseInt(id));
            return true;
        }
    }
    public void showAvailability(Cursor cursor) {
// Stuff them all into a big string

        String[] elements1 = new String[cursor.getCount()];
        int index = 0;
        while (cursor.moveToNext()) {
            StringBuilder builder = new StringBuilder();
            String id = cursor.getString(0);
            String name = cursor.getString(1);
            double weights = cursor.getDouble(2);
            double price = cursor.getDouble(3);
            String description = cursor.getString(4);
            double availability = cursor.getDouble(5);
            builder.append(id).append(": ");
            builder.append(name).append(": ");
            builder.append(weights).append(": ");
            builder.append(price).append(": ");
            builder.append(description).append(": ");
            builder.append(availability).append(": \n");
//            LinearLayout layout = (LinearLayout)findViewById(R.id.l_layout);
//            CheckBox checkBox = new CheckBox(this);
//            layout.addView(checkBox);
            elements1[index++] = builder.toString();

        }
        cursor.close();
        // Display on the screen
        ArrayAdapter<String> adapter = new ArrayAdapter<String>
                (Main4Activity.this,
                        android.R.layout.simple_list_item_multiple_choice,
                        android.R.id.text1, elements1);
        availableShow.setAdapter(adapter);
    }
    public Cursor getAvailableIngredients() {
        /* Perform a managed query . The Activity will
        handle closing and re - querying the cursor
        when needed . */
        SQLiteDatabase db = available.getReadableDatabase();

        Cursor cursor = db.query(TABLE_NAME_AVAILABILITY, FROM, null,
                null, null, null,
                ORDER_BY);

//        Cursor cursor = db.query(TABLE_NAME, null, null, null, null, null,null);
        return cursor;
    }


    }
